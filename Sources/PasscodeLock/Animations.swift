import UIKit

class Animations {
    static func nudge(on onView: UIView) {
        let animation = CABasicAnimation(keyPath: "position")
        animation.duration = 0.095
        animation.repeatCount = 3
        animation.autoreverses = true
        animation.fromValue = NSValue(cgPoint: CGPoint(x: onView.center.x - 15, y: onView.center.y))
        animation.toValue = NSValue(cgPoint: CGPoint(x: onView.center.x + 15, y: onView.center.y))
        onView.layer.add(animation, forKey: "position")
    }
}
