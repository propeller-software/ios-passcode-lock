import UIKit

public struct AuthConfiguration {
    var titleTextColor: UIColor
    var usesBiometrics: Bool
    var numberOfDigits: Int
    var preferredDotSize: CGFloat
    var preferredDashHeight: CGFloat
    var backgroundColor: UIColor
    var preferredTintColor: UIColor

    public init(titleTextColor: UIColor = .black,
                usesBiometrics: Bool = false,
                numberOfDigits: Int = 4,
                preferredDotSize: CGFloat = 20,
                preferredDashHeight: CGFloat = 3,
                backgroundColor: UIColor = .white,
                preferredTintColor: UIColor = .blue) {
        self.titleTextColor = titleTextColor
        self.usesBiometrics = usesBiometrics
        self.numberOfDigits = numberOfDigits
        self.preferredDotSize = preferredDotSize
        self.preferredDashHeight = preferredDashHeight
        self.backgroundColor = backgroundColor
        self.preferredTintColor = preferredTintColor
    }

    static var defaultConfiguration: AuthConfiguration {
        return AuthConfiguration(titleTextColor: .black,
                                 usesBiometrics: true,
                                 numberOfDigits: 4,
                                 preferredDotSize: 20,
                                 preferredDashHeight: 2,
                                 backgroundColor: .white,
                                 preferredTintColor: .blue)
    }
}
