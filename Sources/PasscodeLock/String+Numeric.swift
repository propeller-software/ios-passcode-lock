import Foundation

extension String {

    var isNumeric: Bool {
        let numbers: Set<Character> = ["0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "0"]
        return Set(self).isSubset(of: numbers)
    }
    
}
