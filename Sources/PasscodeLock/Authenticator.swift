import UIKit
import KeychainSwift
import LocalAuthentication

public enum AuthenticatorError: Error {
    case invalidPasscode
    case passcodeNotSet
    case passcodeAlreadyEnabled
    case alreadyAuthenticated
}

public class Authenticator {
    
    public typealias Completion = (AuthenticatorError?) -> ()
    
    // MARK: - Singleton
    private static var _shared: Authenticator?
    
    public static var shared: Authenticator {
        guard let shared = _shared else {
            fatalError("Authenticator is not initialized. Please call Authenticator.initialize() on app start.")
        }
        return shared
    }
    
    /// Initializes and configures the Authenticator.
    /// - Parameters:
    ///   - window: UIWindow handling the application's UI.
    ///   - numberOfDigits: Passcode number of digits.
    ///   - dotSize: Dot radius.
    ///   - dashHeight: Dash height. The width is goind to be the same as the dot radius.
    public static func initialize(window: UIWindow, configuration: AuthConfiguration? = nil) {
        let configuration = configuration ?? AuthConfiguration.defaultConfiguration
        _shared = Authenticator(window: window, configuration: configuration)
    }
    
    // MARK: - Properties
    private let window: UIWindow
    private let authenticatorWindow: UIWindow
    private let viewModel: AuthConfiguration
    
    private let keychain = KeychainSwift()
    private let passcodeKey = "PLPasscodeKey"
    private let completionDelay: TimeInterval = 0.4
    private var lastAuthDate: Date?
    
    private var shouldLock: Bool {
        guard let lastAuthDate = self.lastAuthDate else {
            return true
        }
        
        return Date().timeIntervalSince(lastAuthDate) >= 2
    }
    
    public var isPasscodeEnabled: Bool {
        return currentPasscode != nil
    }

    private var currentPasscode: String? {
        return keychain.get(passcodeKey)
    }
    
    // MARK: - Initializer
    private init(window: UIWindow, configuration: AuthConfiguration) {
        self.window = window
        self.authenticatorWindow = UIWindow(frame: UIScreen.main.bounds)
        self.viewModel = configuration
    }
    
    // MARK: - Helper methods
    func set(passcode: String) {
        keychain.set(passcode, forKey: passcodeKey)
    }

    func removePasscode() {
        keychain.delete(passcodeKey)
    }
    
    private func nudge(viewController: UIViewController? = nil) {
        if let nudgeable = viewController?.navigationController?.topViewController as? AuthenticatorPresentable {
            nudgeable.nudge()
        } else if let nudgeable = (viewController?.presentedViewController as? UINavigationController)?.topViewController as? AuthenticatorPresentable {
            nudgeable.nudge()
        } else if let nudgeable = authenticatorWindow.rootViewController as? AuthenticatorPresentable {
            nudgeable.nudge()
        }
    }
}

// MARK: - Publicly exposed APIs
extension Authenticator {
    
    /// Presents the UI for enabling the passcode lock.
    /// - Parameters:
    ///   - viewController: UIViewController which will present the Authenticator UI
    ///   - completion: AuthenticationError. If the operation is completed, the AuthenticationError is going to be nil
    public func enablePasscode(inViewController viewController: UIViewController, completion: Completion? = nil) {
        guard isPasscodeEnabled == false else {
            completion?(.passcodeAlreadyEnabled)
            return
        }
        
        let enterPasscodeViewController = AuthViewController(viewModel: viewModel, style: .createPasscode) { [self] (passcode) in
            set(passcode: passcode)
            DispatchQueue.main.asyncAfter(deadline: .now() + completionDelay) {
                completion?(nil)
            }
        }
        
        enterPasscodeViewController.configureNavigationItems(isModal: true)
        
        let navigationController = UINavigationController(rootViewController: enterPasscodeViewController)
        
        viewController.present(navigationController, animated: true, completion: nil)
    }
    
    /// Presents the UI for disabling the passcode lock.
    /// - Parameters:
    ///   - viewController: UIViewController which will present the Authenticator UI
    ///   - completion: AuthenticationError. If the operation is completed, the AuthenticationError is going to be nil.
    public func disablePasscode(inViewController viewController: UIViewController, completion: Completion? = nil) {
        guard isPasscodeEnabled else {
            completion?(.passcodeNotSet)
            return
        }
        
        authenticate(inViewController: viewController, isModal: true) { [self] (error) in
            if let error = error {
                completion?(error)
                return
            }
            
            removePasscode()
            DispatchQueue.main.asyncAfter(deadline: .now() + completionDelay) {
                completion?(nil)
            }
        }
    }
    
    public func changePasscode(inViewController viewController: UIViewController, completion: Completion? = nil) {
        guard isPasscodeEnabled, let passcode = self.currentPasscode else {
            completion?(.passcodeNotSet)
            return
        }
        
        let enterPasscodeViewController = AuthViewController(viewModel: viewModel, style: .changePasscode(passcode)) { [self] (passcode) in
            set(passcode: passcode)
            DispatchQueue.main.asyncAfter(deadline: .now() + completionDelay) {
                completion?(nil)
            }
        }
        
        enterPasscodeViewController.configureNavigationItems(isModal: true)
        
        let navigationController = UINavigationController(rootViewController: enterPasscodeViewController)
        
        viewController.present(navigationController, animated: true, completion: nil)
    }
    
    /// Locks the device.
    /// - Parameter completion: AuthenticationError. If the operation is completed, the AuthenticationError is going to be nil
    public func lock(completion: Completion? = nil) {
        
        guard shouldLock == true else {
            completion?(.alreadyAuthenticated)
            return
        }
        
        guard authenticatorWindow.isKeyWindow == false else {
            completion?(.alreadyAuthenticated)
            (authenticatorWindow.topViewController as? AuthViewController)?.focus()
            return
        }
        
        guard isPasscodeEnabled else {
            completion?(.passcodeNotSet)
            return
        }
        
        let authenticated: () -> () = { [self] in
            DispatchQueue.main.asyncAfter(deadline: .now() + completionDelay) {
                self.window.makeKeyAndVisible()
                self.authenticatorWindow.rootViewController = UIViewController()
                self.authenticatorWindow.endEditing(true)
            }
            completion?(nil)
            self.lastAuthDate = Date()
        }
        
        let authViewController = AuthViewController(viewModel: viewModel) { [self] (passcode) in
            if passcode == self.currentPasscode {
                authenticated()
            } else {
                nudge()
                completion?(.invalidPasscode)
            }
        }
        
        authenticatorWindow.rootViewController = authViewController
        authenticatorWindow.makeKeyAndVisible()
        DispatchQueue.main.asyncAfter(deadline: .now() + completionDelay) { [self] in
            authenticateUsingBiometrics { (success, error) in
                if success == true {
                    authenticated()
                }
            }
        }
    }
    
    /// Authenticates the user in UIViewController
    /// - Parameters:
    ///   - viewController: UIViewController which will present the Authenticator UI
    ///   - isModal: Flag indicating whether the UI should be presented modally or pushed to the navigation stack.
    ///   - completion: AuthenticationError. If the operation is completed, the AuthenticationError is going to be nil.
    public func authenticate(inViewController viewController: UIViewController, isModal: Bool = false, completion: @escaping Completion) {
        guard isPasscodeEnabled else {
            DispatchQueue.main.asyncAfter(deadline: .now() + completionDelay) {
                completion(.passcodeNotSet)
            }
            return
        }
        
        let authViewController = AuthViewController(viewModel: viewModel) { [self] (passcode) in
            if passcode == self.currentPasscode {
                DispatchQueue.main.asyncAfter(deadline: .now() + completionDelay) {
                    completion(nil)
                }
            } else {
                nudge(viewController: viewController)
                DispatchQueue.main.asyncAfter(deadline: .now() + completionDelay) {
                    completion(.invalidPasscode)
                }
            }
        }
        
        if isModal {
            authViewController.configureNavigationItems(isModal: true)
            let navigationViewController = UINavigationController(rootViewController: authViewController)
            viewController.present(navigationViewController, animated: true, completion: nil)
        } else {
            viewController.navigationController?.show(authViewController, sender: nil)
        }
    }
    
    private func authenticateUsingBiometrics(completion: @escaping (Bool, Error?) -> ()) {
        let context = LAContext()
        var error: NSError?
        let reason = "Authenticator.Biometrics.Reason".localized

        context.localizedReason = reason
        context.localizedCancelTitle = "Authenticator.Biometrics.Cancel".localized
        context.localizedFallbackTitle = "Authenticator.Biometrics.FallbackTitle".localized
        
        if context.canEvaluatePolicy(.deviceOwnerAuthenticationWithBiometrics, error: &error) {
            context.evaluatePolicy(.deviceOwnerAuthenticationWithBiometrics, localizedReason: reason, reply: completion)
        }
    }
    
}

// MARK: - UIWindow helper
fileprivate extension UIWindow {
    var topViewController: UIViewController? {
        var top = self.rootViewController
        while true {
            if let presented = top?.presentedViewController {
                top = presented
            } else if let nav = top as? UINavigationController {
                top = nav.visibleViewController
            } else if let tab = top as? UITabBarController {
                top = tab.selectedViewController
            } else {
                break
            }
        }
        return top
    }
}
