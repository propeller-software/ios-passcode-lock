import UIKit
import SnapKit

protocol AuthenticatorPresentable {
    func nudge()
}

class AuthViewController: UIViewController, AuthenticatorPresentable {

    enum Style {
        case enterPasscode
        case createPasscode
        case reEnterPasscode(String)
        case changePasscode(String)
        case enterNewPasscode

        var title: String {
            switch self {
            case .enterPasscode, .changePasscode:
                return "Authenticator.Passcode.Enter".localized
            case .createPasscode:
                return "Authenticator.Passcode.Create".localized
            case .reEnterPasscode:
                return "Authenticator.Passcode.ReEnter".localized
            case .enterNewPasscode:
                return "Authenticator.Passcode.EnterNew".localized
            }
        }
    }

    typealias Completion = (String) -> ()

    // MARK: Properties
    private let viewModel: AuthConfiguration
    private let completion: Completion
    private let style: Style

    // MARK: Views
    private var stackView: UIStackView = {
        let stackView = UIStackView()
        stackView.axis = .vertical
        stackView.spacing = 30
        stackView.alignment = .center
        stackView.isLayoutMarginsRelativeArrangement = true
        stackView.layoutMargins = UIEdgeInsets(top: 100, left: 20, bottom: 20, right: 20)
        return stackView
    }()

    private var titleLabel: UILabel = {
        let label = UILabel()
        return label
    }()

    private lazy var passcodeView: PasscodeView = {
        let passcodeView = PasscodeView(numberOfDigits: viewModel.numberOfDigits,
                                        preferredDotSize: viewModel.preferredDotSize,
                                        preferredDashHeight: viewModel.preferredDashHeight)
        passcodeView.delegate = self
        return passcodeView
    }()

    // MARK: - Initializers
    init(viewModel: AuthConfiguration, style: Style = .enterPasscode, completion: @escaping Completion) {
        self.viewModel = viewModel
        self.completion = completion
        self.style = style
        super.init(nibName: nil, bundle: nil)
        self.view.tintColor = viewModel.preferredTintColor
        titleLabel.text = style.title
        titleLabel.textColor = viewModel.titleTextColor
        view.backgroundColor = viewModel.backgroundColor
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - UIController Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        view.addSubview(stackView)
        stackView.addArrangedSubview(titleLabel)
        stackView.addArrangedSubview(passcodeView)

        stackView.snp.makeConstraints { make in
            make.leading.trailing.top.equalToSuperview()
        }
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        passcodeView.focus()
    }

    func focus() {
        passcodeView.focus()
    }

    // MARK: - Public Methods
    func nudge() {
        passcodeView.nudge()
    }

    func configureNavigationItems(isModal: Bool = false) {
        if isModal {
            navigationItem.leftBarButtonItem = UIBarButtonItem(title: "Close", style: .plain, target: self, action: #selector(close))
        } else {
            navigationItem.leftBarButtonItem = nil
        }
    }

    @objc private func close() {
        dismiss(animated: true, completion: nil)
    }

}

// MARK: - PasscodeViewDelegate
extension AuthViewController: PasscodeViewDelegate {

    func passcodeView(_ passcodeView: PasscodeView, didEnterPasscode passcode: String) {
        switch style {
        case .enterPasscode:
            completion(passcode)
        case .createPasscode:
            let viewController = AuthViewController(viewModel: viewModel, style: .reEnterPasscode(passcode), completion: completion)
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.4) {
                self.navigationController?.show(viewController, sender: nil)
            }
        case .reEnterPasscode(let originalPasscode):
            if originalPasscode == passcode {
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.4) {
                    self.completion(passcode)
                }
            } else {
                nudge()
            }
        case .changePasscode(let currentPasscode):
            if passcode == currentPasscode {
                let viewController = AuthViewController(viewModel: viewModel, style: .enterNewPasscode, completion: completion)
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.4) {
                    self.navigationController?.setViewControllers([viewController], animated: true)
                }
            } else {
                nudge()
            }

        case .enterNewPasscode:
            let viewController = AuthViewController(viewModel: viewModel, style: .reEnterPasscode(passcode), completion: completion)
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.4) {
                self.navigationController?.show(viewController, sender: nil)
            }
        }
    }
    
}
