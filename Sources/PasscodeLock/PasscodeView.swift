import UIKit


// MARK: - PasscodeViewDelegate
protocol PasscodeViewDelegate: class {
    func passcodeView(_ passcodeView: PasscodeView, didEnterPasscode passcode: String)
}

class PasscodeView: UIView {

    // MARK: - Configuration
    private let numberOfDigits: Int
    private let preferredDotSize: CGFloat
    private let preferredDashHeight: CGFloat

    // MARK: - Other properties
    weak var delegate: PasscodeViewDelegate?

    var passcode: String = "" {
        didSet {
            updateDots()
            if passcode.count == numberOfDigits {
                delegate?.passcodeView(self, didEnterPasscode: passcode)
            }
        }
    }

    private var dots: [CAShapeLayer] = []
    private var dashes: [CAShapeLayer] = []

    private lazy var textField: UITextField = {
        let input = UITextField()
        input.isHidden = true
        input.keyboardType = .numberPad
        input.delegate = self
        return input
    }()

    // MARK: - Initializers
    init(numberOfDigits: Int, preferredDotSize: CGFloat = 20, preferredDashHeight: CGFloat = 3) {
        self.numberOfDigits = numberOfDigits
        self.preferredDotSize = preferredDotSize
        self.preferredDashHeight = preferredDashHeight

        super.init(frame: .zero)

        addSubview(textField)
        textField.snp.makeConstraints { (make) in
            make.edges.equalToSuperview()
        }

        for _ in 1...numberOfDigits {
            dots.append(CAShapeLayer())
            dashes.append(CAShapeLayer())
        }


        dots.forEach {
            layer.addSublayer($0)
            $0.isHidden = true
        }

        dashes.forEach {
            layer.addSublayer($0)
        }
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - Helper methods
    func focus()  {
        textField.becomeFirstResponder()
    }

    private func updateDots() {
        let numberOfFilledDots = passcode.count
        for i in 0..<numberOfFilledDots {
            dots[i].isHidden = false
            dashes[i].isHidden = true
        }
        for i in numberOfFilledDots..<numberOfDigits {
            dots[i].isHidden = true
            dashes[i].isHidden = false
        }
    }

    func nudge() {
        Animations.nudge(on: self)
        UIDevice.vibrate()
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.57) {
            self.clear()
        }
    }

    func clear() {
        textField.text = nil
        passcode = ""
    }

    // MARK: - UIView stuff
    override func layoutSubviews() {
        superview?.layoutSubviews()

        let dotRadius = bounds.height
        let spacing: CGFloat = (bounds.width - (CGFloat(numberOfDigits) * dotRadius)) / CGFloat(numberOfDigits - 1)
        dots.enumerated().forEach { (index, dot) in
            dot.backgroundColor = tintColor.cgColor
            dot.frame = CGRect(origin: CGPoint(x: CGFloat(index) * spacing + CGFloat(index) * dotRadius, y: 0), size: CGSize(width: dotRadius, height: dotRadius))
            dot.cornerRadius = dotRadius / 2
        }

        dashes.enumerated().forEach { (index, dash) in
            dash.backgroundColor = tintColor.cgColor
            dash.frame = CGRect(origin: CGPoint(x: CGFloat(index) * spacing + CGFloat(index) * dotRadius, y: dotRadius - preferredDashHeight), size: CGSize(width: dotRadius, height: preferredDashHeight))
        }
    }

    override var intrinsicContentSize: CGSize {
        CGSize(width: preferredDotSize * CGFloat(numberOfDigits) + 100, height: preferredDotSize)
    }
    
}

// MARK: - UITextFieldDelegate
extension PasscodeView: UITextFieldDelegate {

    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        guard string.isNumeric else {
            return false
        }
        let passcode = ((textField.text ?? "") as NSString).replacingCharacters(in: range, with: string)

        guard passcode.count <= numberOfDigits else {
            return false
        }

        self.passcode = passcode
        return true
    }

}

